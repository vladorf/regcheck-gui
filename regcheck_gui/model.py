"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Contains data transformations for presentation logic
"""
from __future__ import annotations

import csv
import io
import os.path
from dataclasses import dataclass, field
from typing import List

from regcheck.__main__ import main as regcheck


@dataclass
class Peripheral:
    """Models peripheral device"""
    name: str
    registers: List[Register] = field(default_factory=list)

    def filtered_registers(self, name):
        filtering_fnc = lambda reg: name in reg.name
        return list(filter(filtering_fnc, self.registers))


@dataclass
class Register:
    """Models one register of peripheral"""
    name: str
    address: int
    value: int
    bitfields: List[Bitfield] = field(default_factory=list)


@dataclass
class Bitfield:
    """Models bitfields"""
    name: str
    value: int


@dataclass
class Args:
    """Models arguments for regcheck"""
    function: str
    config: io.TextIO
    diff_only: bool = False
    output_memory: io.StringIO = False
    output_rw_logs: io.StringIO = False
    all_paths: bool = False
    no_cache: bool = False


class Model:

    def __init__(self):
        """Initialize model"""
        self._rw_logs_headers = []
        self._rw_logs_rows = []
        self._peripherals = {}

        self._memory_container = io.StringIO()
        self._rw_logs_container = io.StringIO()
        
        self.populated = False

    def run_regcheck(self, function_name, config_path):
        """Run regcheck analyzer."""
        self._memory_container = io.StringIO()
        self._rw_logs_container = io.StringIO()

        with open(config_path) as config:
            args = Args(function_name, config,
                        output_memory=self._memory_container, output_rw_logs=self._rw_logs_container)
            regcheck(args)
        
        self._populate_peripherals()
        self._populate_rw_logs()
        self.populated = True

    def _populate_peripherals(self):
        """Transform regcheck memory output into hierarchy of dataclasses."""
        self._memory_container.seek(0)
        for line in csv.DictReader(self._memory_container):
            # all names should be in peripheral->register format
            if "->" not in line["name"]:
                continue
            
            peripheral_name, register_name = line["name"].split("->", maxsplit=1)
            peripheral = self._peripherals.setdefault(peripheral_name, Peripheral(peripheral_name))
            
            register = Register(register_name, line["address"], line["value"])
            peripheral.registers.append(register)

            for bitfield in line["bitfields"].split(";"):
                # another weird case
                # TODO: why some bitfields have not `=` in them?
                if "=" in bitfield:
                    name, value = bitfield.split("=")
                    register.bitfields.append(Bitfield(name, value))

    def _populate_rw_logs(self):
        """Tranform regcheck rw-logs output into list"""
        self._rw_logs_container.seek(0)
        data = list(csv.reader(self._rw_logs_container))
        
        headers = data[0]
        headers[0] = "operation"  # rename column - `op` -> `operation`
        
        self._rw_logs_headers = headers
        self._rw_logs_rows = data[1:]

    @staticmethod
    def _filter_by_periph_or_reg(peripherals, filter_key):
        """Filter peripherals or registers based on given string"""
        for idx, periph in enumerate(peripherals):
            if filter_key in periph.name:
                continue

            filtered_regs = periph.filtered_registers(filter_key)
            if filtered_regs:
                new_periph = Peripheral(periph.name, filtered_regs)
                peripherals[idx] = new_periph
                continue

            # filtering peripheral out
            peripherals[idx] = None

        # clean list from peripherals that were filtered out
        return list(filter(lambda periph: periph is not None, peripherals))

    @staticmethod
    def _filter_by_both(peripherals, filter_key):
        """Filter both peripherals and registers with given string"""
        filter_periph_name, filter_reg_name = filter_key.split(".")

        for idx, periph in enumerate(peripherals):
            if filter_periph_name not in periph.name:
                peripherals[idx] = None
                continue

            # if its something like ISC0. print all registers for given periphs
            if filter_reg_name:
                filtered_regs = periph.filtered_registers(filter_reg_name)
                if filtered_regs:
                    new_periph = Peripheral(periph.name, filtered_regs)
                    peripherals[idx] = new_periph
                else:
                    peripherals[idx] = None
            
        # clean list from peripherals that were filtered out
        return list(filter(lambda periph: periph is not None, peripherals))

    def peripherals(self, filter_key):
        """Return hierarchy of dataclasses modeling regcheck output. Can be filtered."""
        if not self.populated:
            raise Exception("Method run_regcheck was not called yet")

        peripherals = list(self._peripherals.values())

        if filter_key:
            # substring match by both periph and reg name
            # e.g. ISC.A -> ISC0->A1, ISC1->A1,
            filter_key = filter_key.upper()
            if "." in filter_key:
                return self._filter_by_both(peripherals, filter_key)
            else:
                return self._filter_by_periph_or_reg(peripherals, filter_key)
        
        return peripherals

    def rw_logs(self, filter_register_name):
        """Return read-write logs given by regcheck analyzator. Can be filtered."""
        if not self.populated:
            raise Exception("Method run_regcheck was not called yet")

        headers = self._rw_logs_headers
        rows = self._rw_logs_rows

        if filter_register_name:
            register_name_idx = headers.index("name")
            filtering_fnc = lambda row: filter_register_name in row[register_name_idx]
            rows = list(filter(filtering_fnc, rows))
        
        return headers, rows
        
    def export_peripherals(self, dest):
        """Export register values into file."""
        self._memory_container.seek(0)

        with open(os.path.join(dest, "memory.csv"), "w") as f:
            f.write(self._memory_container.getvalue())

    def export_rw_logs(self, dest):
        """Export read write logs into file."""
        self._rw_logs_container.seek(0)
        
        with open(os.path.join(dest, "rw_logs.csv"), "w") as f:
            f.write(self._rw_logs_container.getvalue())

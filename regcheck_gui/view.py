"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Module containing presentation logic of GUI.
"""
from subprocess import CalledProcessError

import wx

from generated_view import MyFrame1
from regcheck_gui.model import Model


class Frame(MyFrame1):
    def __init__(self, *args):
        """Initialize view."""
        super().__init__(*args)

        self.rw_logs_list = wx.ListCtrl(self.m_panel4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_REPORT)
        self.m_panel4.GetSizer().Add(self.rw_logs_list, 1, wx.EXPAND |wx.ALL, 5)

        self.status_bar.SetStatusText("Ready")
        self.memory = None
        self.rw_logs = None

        self.model = Model()

    def run_regcheck(self, event):
        """Run button handle"""
        function_name = self.function_name_input.GetLineText(0)
        path = self.config_picker.GetPath()
        if not function_name or not path:
            wx.MessageBox("Both function name and configuration file path must be set.", 'Error', wx.OK|wx.ICON_ERROR)
            return

        self.status_bar.SetStatusText("Processing...")
        
        try:
            self.model.run_regcheck(function_name, path)
        except CalledProcessError as exc:
            wx.MessageBox(f"Preprocessing of C files failed\n\nStderr:\n{exc.stderr.decode()}{exc.stdout.decode()}", "Error", wx.OK|wx.ICON_ERROR)
            self.status_bar.SetStatusText(f"Error: {exc}")
        except Exception as exc:
            wx.MessageBox(f"Regcheck failed: {repr(exc)}", "Error", wx.OK|wx.ICON_ERROR)
            self.status_bar.SetStatusText(f"Error: {exc}")
        else:
            self.make_memory_tree()
            self.make_rw_logs_list()
            self.status_bar.SetStatusText("Ready")

    def make_memory_tree(self, filter_register_name=""):
        """Populate TreeCtrl containing register values."""
        self.result_tree.DeleteAllItems()
        root = self.result_tree.AddRoot("")

        peripherals = self.model.peripherals(filter_register_name)

        for peripheral in peripherals:
            peripheral_root = self.result_tree.AppendItem(root, peripheral.name)
            
            for register in peripheral.registers:
                register_root = self.result_tree.AppendItem(peripheral_root, register.name)
                
                self.result_tree.AppendItem(register_root, f"address = {register.address}")
                self.result_tree.AppendItem(register_root, f"value = {register.value}")
                
                bitfield_root = self.result_tree.AppendItem(register_root, "bitfields")
                
                for bitfield in register.bitfields:
                    self.result_tree.AppendItem(bitfield_root, f"{bitfield.name} = {bitfield.value}")

        if filter_register_name:
            self.result_tree.ExpandAll()

    def make_rw_logs_list(self, filter_register_name=""):
        """Populate ListCtrl containing read-write logs."""
        self.rw_logs_list.ClearAll()

        headers, rows = self.model.rw_logs(filter_register_name)

        for header in headers:
            self.rw_logs_list.AppendColumn(header)
            
        for row in rows:
            self.rw_logs_list.Append(row)

        if rows:
            # autosize columns to fit data in rows
            for col in range(1, self.rw_logs_list.GetColumnCount()):
                self.rw_logs_list.SetColumnWidth(col, wx.LIST_AUTOSIZE)

    def run_search(self, event):
        """OnChange search handle"""
        value = self.search_bar.GetValue()
        if self.model.populated:
            self.status_bar.SetStatusText("Searching...")
            self.make_memory_tree(value)
            self.make_rw_logs_list(value)
            self.status_bar.SetStatusText("Ready")

    def clear_search(self, event):
        """Clear button handle."""
        self.search_bar.SetValue("")
        self.make_memory_tree("")
        self.make_rw_logs_list("")

    def export_files(self, event):
        """Export button handle."""
        dest = self.export_dest.GetPath()
        if not dest:
            wx.MessageBox("Destination directory for export must be set.", 'Error', wx.OK|wx.ICON_ERROR)
            return

        self.model.export_peripherals(dest)
        self.model.export_rw_logs(dest)

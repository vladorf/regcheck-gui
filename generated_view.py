# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Mar 22 2020)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MyFrame1
###########################################################################

class MyFrame1 ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Regcheck GUI", pos = wx.DefaultPosition, size = wx.Size( 500,491 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Function name" ), wx.VERTICAL )

		self.function_name_input = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.function_name_input.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )

		sbSizer1.Add( self.function_name_input, 0, wx.ALL|wx.EXPAND, 5 )


		bSizer1.Add( sbSizer1, 0, wx.EXPAND, 5 )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Configuration file path" ), wx.VERTICAL )

		self.config_picker = wx.FilePickerCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.ini", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		sbSizer2.Add( self.config_picker, 0, wx.ALL|wx.EXPAND, 5 )


		bSizer1.Add( sbSizer2, 0, wx.EXPAND, 5 )

		bSizer2 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button1 = wx.Button( self, wx.ID_ANY, u"Run", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.m_button1, 0, wx.ALL, 5 )


		bSizer2.Add( bSizer3, 1, 0, 5 )

		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

		self.export_dest = wx.DirPickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE|wx.DIRP_DIR_MUST_EXIST )
		bSizer4.Add( self.export_dest, 0, wx.ALL, 5 )

		self.m_button3 = wx.Button( self, wx.ID_ANY, u"Export CSV", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.m_button3, 0, wx.ALL, 5 )


		bSizer2.Add( bSizer4, 0, 0, 5 )


		bSizer1.Add( bSizer2, 0, wx.EXPAND, 5 )

		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Result" ), wx.VERTICAL )

		self.search_bar = wx.SearchCtrl( sbSizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.search_bar.ShowSearchButton( True )
		self.search_bar.ShowCancelButton( True )
		sbSizer3.Add( self.search_bar, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_notebook2 = wx.Notebook( sbSizer3.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NB_TOP )
		self.m_panel1 = wx.Panel( self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer6 = wx.BoxSizer( wx.VERTICAL )

		self.result_tree = wx.TreeCtrl( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TR_DEFAULT_STYLE|wx.TR_HIDE_ROOT )
		bSizer6.Add( self.result_tree, 1, wx.ALL|wx.EXPAND, 5 )


		self.m_panel1.SetSizer( bSizer6 )
		self.m_panel1.Layout()
		bSizer6.Fit( self.m_panel1 )
		self.m_notebook2.AddPage( self.m_panel1, u"Memory", False )
		self.m_panel4 = wx.Panel( self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer61 = wx.BoxSizer( wx.VERTICAL )


		self.m_panel4.SetSizer( bSizer61 )
		self.m_panel4.Layout()
		bSizer61.Fit( self.m_panel4 )
		self.m_notebook2.AddPage( self.m_panel4, u"RW Logs", False )

		sbSizer3.Add( self.m_notebook2, 1, wx.EXPAND |wx.ALL, 5 )


		bSizer1.Add( sbSizer3, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		self.status_bar = self.CreateStatusBar( 1, wx.STB_SIZEGRIP, wx.ID_ANY )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button1.Bind( wx.EVT_BUTTON, self.run_regcheck )
		self.m_button3.Bind( wx.EVT_BUTTON, self.export_files )
		self.search_bar.Bind( wx.EVT_SEARCHCTRL_CANCEL_BTN, self.clear_search )
		self.search_bar.Bind( wx.EVT_SEARCHCTRL_SEARCH_BTN, self.run_search )
		self.search_bar.Bind( wx.EVT_TEXT, self.run_search )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def run_regcheck( self, event ):
		event.Skip()

	def export_files( self, event ):
		event.Skip()

	def clear_search( self, event ):
		event.Skip()

	def run_search( self, event ):
		event.Skip()




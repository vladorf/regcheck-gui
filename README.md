## Regcheck-gui

Graphical user interface for [regcheck](https://gitlab.com/vladorf/regcheck) analyzator.
Based on wxWidgets.

## Installation
Needs at least python >= 3.7.

Firstly you need to build regcheck, as it is needed for regcheck-gui to work.
Then it can be installed
```bash
cd regcheck
make build
cd ../regcheck-gui
python3 -m venv venv
source venv/bin/activate
pip3 install ../regcheck/dist/regcheck-0.0.0-py3-none-any.whl
pip3 install -r requirements.txt
```

Regcheck is installed separately because we are using local
wheel (also because it is not part of python central repository).

Notes:
+ initial installation of requirements may take a while as
C++ part of wxWidgets is compiled during this process

## Run

Regcheck-gui can be run `python3 -m regcheck_gui` from its directory.
Beware, regcheck examples won't work as they contain relative paths and work only
from regcheck directory.
If you want to run them, you have to run recheck_gui from regcheck directory.

```bash
cd ../regcheck
PYTHONPATH="$(pwd)/../regcheck-gui" python3 -m regcheck_gui
```
PYTHONPATH temporarily adds regcheck-gui to python's searchpath.

Om windows most safe option is to use absolute paths.

## Build windows executable

Regcheck-gui can be built as windows executable using pyInstaller. Unfortunately
it doesn't do cross-compilation so we need to produce windows environment inside linux.
We will use prepared docker image for that (it saves us the trouble of setting up pyInstaller with wine).

Regcheck and regcheck-gui needs to share same parent directory (otherwise relative paths won't work)

```bash
docker run --rm -v "$(pwd):/src/" 
-v "$(python3 -c "from importlib.util import find_spec; print(find_spec(\"fake_libc_include\").submodule_search_locations[0])"):/src/fake_libc_include" 
-v "$(pwd)/../regcheck/dist/regcheck-0.0.0-py3-none-any.whl:/regcheck-0.0.0-py3-none-any.whl" 
--entrypoint /bin/sh cdrx/pyinstaller-windows:python3  -c "pip install /regcheck-0.0.0-py3-none-any.whl  && /entrypoint.sh
```

Result can be found in `dist/windows/regcheck.exe`


